use image::RgbImage;
use std::env;
use std::path::{Path, PathBuf};

fn main() {
    // parse args to get the image paths
    let mut it = env::args().skip(1);
    let mut parse_next = move || {
        PathBuf::from(
            it.next()
                .expect("expected two command-line arguments [first, second]"),
        )
        .canonicalize()
        .expect("not a valid directory")
    };

    let open = |path: &Path| {
        image::open(&path)
            .expect(&format!(
                "could not open the image at {}",
                path.canonicalize().expect("canonicalizing path").display()
            ))
            .to_rgb8()
    };
    let (l_path, r_path) = (parse_next(), parse_next());
    let (left, right) = (open(&l_path), open(&r_path));
    // check if images are comparable: same dimensions
    let (width, height) = left.dimensions();
    let (rw, rh) = right.dimensions();
    assert!(
        (width, height) == (rw, rh),
        concat!(
            "expected the images to have the same dimensions, but",
            "\n\t{lpath} was {lw:04}x{lh:04}",
            "\n\t{rpath} was {rw:04}x{rh:04}",
        ),
        lpath = l_path.display(),
        rpath = r_path.display(),
        lw = width,
        lh = height,
        rw = rw,
        rh = rh,
    );

    // compare images pixel-by-pixel, storing absolute difference in output
    let mut output = RgbImage::new(width, height);
    let src = left
        .pixels()
        .map(|rgb| rgb.0)
        .zip(right.pixels().map(|rgb| rgb.0));

    for (([r0, g0, b0], [r1, g1, b1]), dst_p) in src.zip(output.pixels_mut()) {
        fn abs_diff(a: u8, b: u8) -> u8 {
            i16::abs(i16::from(a) - i16::from(b)) as u8
        }
        dst_p.0 = [abs_diff(r0, r1), abs_diff(g0, g1), abs_diff(b0, b1)];
    }

    let total_diffs = output.pixels().filter(|p| p.0 != [0, 0, 0]).count();
    if total_diffs == 0 {
        eprintln!("no difference found");
        return;
    }
    // if any differences exist, save them to cmp_file1_file2.png
    eprintln!("found {} differences", total_diffs);
    let dst = PathBuf::from(format!(
        "cmp_{}_{}.png",
        l_path.file_stem().unwrap().to_string_lossy(),
        r_path.file_stem().unwrap().to_string_lossy()
    ));
    eprintln!("saving to {}", dst.display());
    output
        .save(&dst)
        .expect(&format!("could not save to {}", dst.display(),));
    eprintln!("saved image to {}", dst.display());
    return;
}
