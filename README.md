# imgcompare
compare two images of the same size, creating a false color image where their pixels differ

## usage
`imgcompare path_to_img1 path_to_img2`